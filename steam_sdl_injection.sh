#!/usr/bin/env sh

export LD_PRELOAD='sdl_block_screensaver_inhibit.so'
export SDL_VIDEO_ALLOW_SCREENSAVER=1
"$@"
